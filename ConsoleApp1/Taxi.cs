﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace ConsoleApp1
{
    public class Taxi
    {
        public int Id { get; private set; }
        List<Corsa> corse = new List<Corsa>();
        public int Bonus { get; private set; }
        public int Score { get; private set; }
        //Last
        public Corsa last { get; private set; }

        public Taxi(int id,int bonus)
        {
            Id = id;
            Bonus = bonus;
            corse.Add(new Corsa(-1, new System.Drawing.Point(0, 0), new System.Drawing.Point(0, 0), 0, 0));
            last = corse.FindLast(x=>x.id==-1);
        }
        public Point CurrentPosition() => last.arrivo;

        public void AddCorsa(Corsa c)
        {
            c.tempoPartenzaVero = ProbabilitaBonus(c) >= 0 ? c.tempoPartenzaMin : c.tempoPartenzaMin + Math.Abs(ProbabilitaBonus(c));
            c.tempoArrivoVero = c.tempoPartenzaVero + c.distanza;
            Score += ProbabilitaBonus(c) >= 0 ? Bonus + c.distanza : c.distanza;
            corse.Add(c);
            last = c;
        }

        public int ProbabilitaBonus(Corsa c)
        {
            return c.tempoPartenzaMin - last.tempoArrivoVero - DistanzaVerso(c);
        }

        public bool IsFeasable(Corsa c)
        {
            return c.tempoArrivoMax - last.tempoArrivoVero - DistanzaVerso(c) - c.distanza >= 0;
        }

        public int DistanzaVerso(Corsa c)
        {
            return Math.Abs(c.partenza.Y - last.arrivo.Y) + Math.Abs(c.partenza.X - last.arrivo.X);
        }

        public String ToString()
        {
            List<Corsa> toProcess = new List<Corsa>(corse);
            toProcess.Remove(toProcess.First());
            String s = String.Format("{0}", toProcess.Count);
            toProcess.ForEach(toP => s += String.Format(" {0}", toP.id));
            return s;
        }
    }
}
