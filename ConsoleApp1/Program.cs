﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Minchia fai?!!!");
                Console.ReadKey();
                return;
            }
            StreamReader input = File.OpenText(args[0]);
            int rows, columns, veicoli, corse, bonus, numIterazioni;
            var primariga = input.ReadLine().Split(" ");
            rows = Int32.Parse(primariga[0]);
            columns = Int32.Parse(primariga[1]);
            veicoli = Int32.Parse(primariga[2]);
            corse = Int32.Parse(primariga[3]);
            bonus = Int32.Parse(primariga[4]);
            numIterazioni = Int32.Parse(primariga[5]);
            List<Corsa> listaCorse = new List<Corsa>();
            int idCorse=0;
            while (!input.EndOfStream)
            {
                var riga = input.ReadLine().Split(" ");
                int row1 = Int32.Parse(riga[0]);
                int col1 = Int32.Parse(riga[1]);
                int row2 = Int32.Parse(riga[2]);
                int col2 = Int32.Parse(riga[3]);
                int earlStart = Int32.Parse(riga[4]);
                int earlFin = Int32.Parse(riga[5]);
                if (row1 < 0 || col1 < 0 || row2 < 0 || col2 < 0 || row1 > rows || row2 > rows || col2 > columns || col1 > columns)
                    continue;
                listaCorse.Add(new Corsa(idCorse++, new Point(row1, col1), new Point(row2, col2), earlStart, earlFin));
            }
            input.Close();
            List<Taxi> taxis = new List<Taxi>();
            for(int i = 0; i < veicoli; i++)
            {
                taxis.Add(new Taxi(i,bonus));
            }
            var pm = PuntoMedio(taxis);
            var cartProductOrdered = (from corsa in listaCorse
                                     from taxi in taxis
                                     select new { corsa, taxi } into tmp
                                     orderby tmp.corsa.DistanzaPartenzaDa(pm) ascending, //ordinamento per distanza dal centro
                                             tmp.corsa.distanza descending
                                     group tmp by tmp.corsa).ToList();
            bool selected = true;
            while (selected)
            {
                selected = false;
                List<Taxi> toAssign = new List<Taxi>(taxis);
                cartProductOrdered.ToList().ForEach(corsa =>
                {
                    if (toAssign.Count == 0)
                        return;
                    var selectedTaxi = (from taxiList in corsa
                                        where taxiList.taxi.IsFeasable(taxiList.corsa)
                                        orderby Math.Abs(taxiList.taxi.ProbabilitaBonus(taxiList.corsa)) ascending
                                        select taxiList).FirstOrDefault();
                    if (selectedTaxi != null)
                    {
                        selectedTaxi.taxi.AddCorsa(selectedTaxi.corsa);
                        cartProductOrdered.Remove(corsa);
                        toAssign.Remove(selectedTaxi.taxi);
                        selected = true;
                    }
                });
                var pMed = PuntoMedio(taxis);
                cartProductOrdered = (from ord in cartProductOrdered
                                     orderby ord.Key.DistanzaPartenzaDa(pMed) descending,
                                     ord.Key.distanza descending
                                     select ord).ToList();
                                     
            }
            StreamWriter output = File.CreateText(args[1]);
            taxis.ForEach(x=>output.WriteLine(x.ToString()));
            output.Close();
            Console.WriteLine("Total score {0}", taxis.Select(x => x.Score).Sum());
            Console.ReadKey();
        }
        public static Point PuntoMedio(List<Taxi> taxis)
        {
            int xM = taxis.Select(x => x.last.partenza.X).Sum() / taxis.Count;
            int yM = taxis.Select(x => x.last.partenza.Y).Sum() / taxis.Count;
            return new Point(xM, yM);
        }
    }
}
