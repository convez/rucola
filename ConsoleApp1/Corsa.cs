﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace ConsoleApp1
{
    public class Corsa
    {
        public int id { get; set; }

        public Point partenza { get; set; }
        public Point arrivo { get; set; }

        public int tempoPartenzaMin { get; set; }
        public int tempoPartenzaVero { get; set; }
        
        public int tempoArrivoMax { get; set; }
        public int tempoArrivoVero { get; set; }

        public int distanza { get; set; }

        public Corsa(int id, Point partenza, Point arrivo, int tempoPartenzaMin, int tempoArrivoMax)
        {
            this.id = id;
            this.partenza = partenza;
            this.arrivo = arrivo;
            this.tempoPartenzaMin = tempoPartenzaMin;
            this.tempoArrivoMax = tempoArrivoMax;
            distanza = Math.Abs(arrivo.Y - partenza.Y) + Math.Abs(arrivo.X - partenza.X);
        }
        public int DistanzaPartenzaDa(Point p)
        {
            return Math.Abs(p.Y - partenza.Y) + Math.Abs(p.X - partenza.X);
        }
    }
}
